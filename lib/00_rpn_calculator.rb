class RPNCalculator
  attr_reader :calculator

  def initialize
    @calculator = []
  end

  def push(num)
    @calculator << num
  end

  def plus
    raise_error if @calculator[-1].nil? || @calculator[-2].nil?
    num2 = @calculator.pop
    num1 = @calculator.pop
    result = num1 + num2
    @calculator << result
  end

  def minus
    raise_error if @calculator[-1].nil? || @calculator[-2].nil?
    num2 = @calculator.pop
    num1 = @calculator.pop
    result = num1.to_f - num2.to_f
    @calculator << result
  end

  def times
    raise_error if @calculator[-1].nil? || @calculator[-2].nil?
    num2 = @calculator.pop
    num1 = @calculator.pop
    result = num1.to_f * num2.to_f
    @calculator << result
  end

  def divide
    raise_error if @calculator[-1].nil? || @calculator[-2].nil?
    num2 = @calculator.pop
    num1 = @calculator.pop
    result = num1.to_f / num2.to_f
    @calculator << result
  end

  def value
    @calculator.last
  end

  def raise_error
    raise 'calculator is empty'
  end

  def tokens(str)
    functions = %(+ - * /)
    str.split(' ').reduce([]) do |token_arr, char|
      if functions.include?(char)
        token_arr << char.to_sym
      else
        token_arr << char.to_i
      end
    end
  end

  def evaluate(str)
    tokens(str).each do |el|
      if el.is_a?(Integer)
        @calculator << el
      elsif el.is_a?(Symbol)
        case el
        when :+ then plus
        when :- then minus
        when :* then times
        when :/ then divide
        end
      end
    end
    value
  end

end
